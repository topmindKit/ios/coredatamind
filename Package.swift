// swift-tools-version:5.0

import PackageDescription

let package = Package(
    name: "CoreDataMind",
    platforms: [
        .macOS(.v10_12), .iOS(.v10), .watchOS(.v4), .tvOS(.v10)
    ],
    products: [        
        .library(name: "CoreDataMind", targets: ["CoreDataMind"])
    ],
    dependencies: [

    ],
    targets: [
        .target(name: "CoreDataMind", dependencies: []),
        .testTarget(name: "CoreDataMindTests", dependencies: ["CoreDataMind"]),
    ]
)
